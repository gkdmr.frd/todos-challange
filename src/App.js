import React from 'react';
import './App.scss';
import Header from './components/Header';
import TodoList from "./components/TodoList";

class App extends React.Component {

    render() {
        return (
            <div className="App">
                <Header title="Todo App"/>
                <TodoList/>
            </div>
        );
    }
}

export default App;
