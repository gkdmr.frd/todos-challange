import React from "react";
import {FaTrash} from 'react-icons/fa';
import "./TodoListItem.scss";

export default ({content, completed, onToggleComplete, onClickRemove}) => {
    return <section  onClick={onToggleComplete} id="TodoListItem">
        &nbsp;<button onClick={onClickRemove}><FaTrash style={{color:"red"}}/></button> <strong style={{
            textDecoration: completed ? "line-through" : ""
        }}>{content}</strong>
    </section>;
};