import React from "react";
import shortId from "shortid";
import {FaPlus} from 'react-icons/fa';
import "./TodoForm.scss";

class TodoForm extends React.Component {

    state = {
        content: ""
    };

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        if(!this.state.content){
            return;
        }
        this.props.onSubmit({
            id: shortId.generate(),
            content: this.state.content,
            completed: false
        });
        this.setState({content: ""});
    };

    render() {
        return <section id="TodoForm">
            <form onSubmit={this.handleFormSubmit}>
                <input type="text" name="content" className="add-todo-input" value={this.state.content}
                       onChange={this.handleChange}
                       placeholder="Type a todo task.."/>
                <button className="add-todo-button" type="submit"><FaPlus/></button>
            </form>
        </section>
    }

}

export default TodoForm;