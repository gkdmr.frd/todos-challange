import React from "react";
import TodoForm from "./TodoForm";
import TodoListItem from "./TodoListItem";
import "./TodoList.scss";

class TodoList extends React.Component {
    state = {
        todos: [],
        filter: "all"
    };

    componentDidMount() {
        this.loadTodos();
    }

    saveTodos = ()=> {
        localStorage.setItem("todos", JSON.stringify(this.state.todos));
    };

    loadTodos = async () => {
        let todos = localStorage.getItem("todos");

        if(todos){
            todos = JSON.parse(todos);
            this.setState({todos});
        }
    };

    addTodo = (todo) => {
        this.setState({
            todos: [todo, ...this.state.todos]
        }, () => this.saveTodos());
    };

    removeTodo = (id) => {
        const todos = [...this.state.todos];
        const index = todos.findIndex(item => item.id === id);
        if (index !== -1) {
            todos.splice(index, 1);
            this.setState({todos}, ()=> this.saveTodos());
        }
    };

    toggleComplete = (id) => {
        const todos = [...this.state.todos];
        const index = todos.findIndex(item => item.id === id);
        if (index !== -1) {
            todos[index]['completed'] = !todos[index]['completed'];
            this.setState({todos}, this.saveTodos);
        }
    };

    filterTodos = (type = "all") => {
        this.setState({
            filter: type
        });
    };


    render() {
        return <section id="TodoList">
            <TodoForm onSubmit={this.addTodo}/>
            <div>
                <button onClick={() => this.filterTodos()}>All</button>
                <button onClick={() => this.filterTodos("active")}>Active</button>
                <button onClick={() => this.filterTodos("completed")}>Completed</button>
            </div>
            <br/>
            {
                !this.state.todos.length ? <div>No todos created yet!</div> :
                    this.state.todos.filter(item => {
                        switch (this.state.filter) {
                            case "active":
                                return item.completed === false;
                            case "completed":
                                return item.completed === true;
                            default:
                                return true;
                        }

                    }).map(todo =>
                        <TodoListItem onToggleComplete={() => this.toggleComplete(todo.id)}
                                      onClickRemove={() => this.removeTodo(todo.id)}
                                      completed={todo.completed}
                                      key={todo.id}
                                      content={todo.content}/>
                    )}

        </section>
    }
}

export default TodoList;